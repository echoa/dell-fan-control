# dell-fan-control

This repo requires https://github.com/TomFreudenberg/dell-bios-fan-control

aur pkg https://aur.archlinux.org/packages/dell-bios-fan-control-git

based on https://github.com/grmat/amdgpu-fancontrol

Simple bash scripts and systemd services to control dell laptops fan pwm. Adjust temp/pwm values and hysteresis/interval in the script as desired. Other adjustments, such as the correct hwmon path might be required as well.

Please don't just run it naively and keep in mind that I'm not responsible for failures, test and make sure this works for you before relying on it

PWM Values should be left alone as dell does not support anything but those 3
____________________
# If you have a single fan system use dell_cpufan.service
 adjust these values in /usr/bin/dell_cpufans
 set temps (in degrees C * 1000) and corresponding pwm values in ascending order and with the same amount of values

TEMPS1=(60000 70000 80000)

 hwmon paths, hardcoded for one gpu and cpu, adjust as needed

FILE_PWM1=$(echo /sys/class/hwmon/hwmon4/pwm1) #cpu fan pwm

FILE_TEMP1=$(echo /sys/class/hwmon/hwmon4/temp1_input) #cpu temp

FILE_TEMP2=$(echo /sys/class/hwmon/hwmon3/temp1_input) #gpu temp

____________________
# If you have dual fan you will need to use dell_gpufan.service also
 adjust these in /usr/bin/dell_gpufans
 set temps (in degrees C * 1000) and corresponding pwm values in ascending order and with the same amount of values

TEMPS2=(60000 60000 70000)

 hwmon paths, hardcoded for one gpu card, adjust as needed
 
FILE_PWM2=$(echo /sys/class/hwmon/hwmon4/pwm2) #gpu pwm

FILE_TEMP2=$(echo /sys/class/hwmon/hwmon3/temp1_input) #gpu temp
